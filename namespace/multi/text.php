<?php
namespace text;

function multi($word, $howManyTimes)
{
    for ($i = 0; $i < $howManyTimes; $i++) {
        echo $word . '<br>';
    }
}

const SAME_CONST = 'text/123';

class A {
    public static function whereAmI()
    {
        echo __NAMESPACE__ . '<br>';
    }
}