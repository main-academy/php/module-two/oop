<?php

class Autoload {
    static public function loader($class) {
        $path = '';
        $class = explode('\\', $class);
        $className = end($class);
        foreach ($class as $pathParts) {
            if ($pathParts == $className) {
                continue;
            }

            $path .= strtolower($pathParts) . '/';
        }

        include $path . $className . '.php';
    }
}