<?php
//namespace MainAcademy\FirstNamespace;
//
//require_once 'firstnamespace/A.php';
//require_once 'secondnamespace/A.php';
//
//$firstA = new FirstNamespace\A();
//
//$firstA = new FirstNamespace\A();
//$secondA = new SecondNamespace\A();
//
//$firstA::whoAmI();
//$secondA::whoAmI();
//
//$firstA = new MainAcademy\FirstNamespace\A();
//$secondA = new MainAcademy\SecondNamespace\A();
//
//$firstA::whoAmI();
//$secondA::whoAmI();
//
//use MainAcademy\FirstNamespace;
//use MainAcademy\SecondNamespace;
//use MainAcademy\{FirstNamespace, SecondNamespace};
//
//$firstA = new FirstNamespace\A();
//$secondA = new SecondNamespace\A();
//
//$firstA::whoAmI();
//$secondA::whoAmI();
//
//use MainAcademy\FirstNamespace as First;
//use MainAcademy\SecondNamespace as Second;
//
//$firstA = new First\A();
//$secondA = new Second\A();
//
//$firstA::whoAmI();
//$secondA::whoAmI();
//
//use MainAcademy\FirstNamespace\A as FirstA;
//use MainAcademy\SecondNamespace\A as SecondA;
//
//$firstA = new FirstA();
//$secondA = new SecondA();
//
//$firstA::whoAmI();
//$secondA::whoAmI();
//
//use MainAcademy\FirstNamespace\A;
//use MainAcademy\SecondNamespace\A as OtherA;
//
//$firstA = new A();
//$secondA = new OtherA();
//
//$firstA::whoAmI();
//$secondA::whoAmI();

//spl_autoload_register(function ($class) {
//    $path = '';
//    $class = explode('\\', $class);
//    $className = end($class);
//    foreach ($class as $pathParts) {
//        if ($pathParts == $className) {
//            continue;
//        }
//
//        $path .= strtolower($pathParts) . '/';
//    }
//
//    include $path . $className . '.php';
//});
//
//function __autoload($class)
//{
//    $path = '';
//    $class = explode('\\', $class);
//    $className = end($class);
//    foreach ($class as $pathParts) {
//        if ($pathParts == $className) {
//            continue;
//        }
//
//        $path .= strtolower($pathParts) . '/';
//    }
//
//    include $path . $className . '.php';
//}

include 'Autoload.php';
spl_autoload_register('Autoload::loader');

use MainAcademy\FirstNamespace\A as FirstA;
use MainAcademy\SecondNamespace\A as SecondA;

try {
    $a = FirstA::whoAmI();
} catch (Exception $exception) {
    echo 'Exception Catched ';
    echo $exception->getMessage();
} finally {
    $a = StdClass();
}

$a::whoAmI();
var_dump(new FirstA());
var_dump(new SecondA());