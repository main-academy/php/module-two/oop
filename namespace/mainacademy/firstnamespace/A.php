<?php

namespace MainAcademy\FirstNamespace;

use http\Exception;
const MYCONST = 'MainAcademy\FirstNamespace\MYCONST';

function someFunction()
{
    echo 'run simple function someFunction() from FirstNamespace';
    echo '<br>';
}

class A
{
    public static function whoAmI()
    {
        throw new \Exception('Message from bar().');
    }
}