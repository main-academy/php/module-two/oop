<?php

namespace MainAcademy\SecondNamespace;

const MYCONST = 'MainAcademy\SecondNamespace\MYCONST';

function someFunction()
{
    echo 'run simple function someFunction() from SecondNamespace';
    echo '<br>';
}

class A
{
    public static function whoAmI()
    {
        echo 'run static function whoAmI() from SecondNamespace A class';
        echo '<br>';
    }
}