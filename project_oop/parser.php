<?php

function createHtmlDocument($data)
{
    $htmlDocument = '<!DOCTYPE html><html>';
    $htmlDocument .= prepareHead();
    $htmlDocument .= prepareBody($data);
    $htmlDocument .= '</html>';

    return $htmlDocument;
}

function prepareHead()
{
    return '<title>Main Academy Project to create own CV</title>' .
        '<meta charset="UTF-8">' .
        '<meta name="viewport" content="width=device-width, initial-scale=1">' .
        '<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">' .
        '<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto">' .
        '<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">' .
        '<style>' .
            'html,body,h1,h2,h3,h4,h5,h6 {font-family: "Roboto", sans-serif}' .
        '</style>';
}

function prepareFooter()
{
    return '<footer class="w3-container w3-teal w3-center w3-margin-top">
        <p>Find me on social media.</p>
        <i class="fa fa-facebook-official w3-hover-opacity"></i>
        <i class="fa fa-instagram w3-hover-opacity"></i>
        <i class="fa fa-twitter w3-hover-opacity"></i>
        <i class="fa fa-linkedin w3-hover-opacity"></i>
        <p>Powered by MainAcademy</p>
    </footer>';
}

function prepareBody($data)
{
    $body = '<body class="w3-light-grey">
                <div class="w3-content w3-margin-top" style="max-width:1400px;">
                    <div class="w3-row-padding">
                        <div class="w3-third">
                            <div class="w3-white w3-text-grey w3-card-4">' .
                                prepareNameAndAvatar($data) .
                                '<div class="w3-container">' .
                                    preparePersonalInformation($data) .
                                    '<hr>' .
                                    prepareSkills($data) .
                                    '<br>' .
                                    prepareLanguages($data) .
                                    '<br>
                                </div>
                            </div><br>
                        </div>
                        <div class="w3-twothird">
                            <div class="w3-container w3-card w3-white w3-margin-bottom">' .
                                prepareExperience($data) .
                            '</div>
                            <div class="w3-container w3-card w3-white">' .
                                prepareEducation($data) .
                            '</div>
                        </div>
                    </div>
                </div>' .
                    prepareFooter() .
            '</body>';

    return $body;
}

function prepareNameAndAvatar($data)
{
    $name = $data[0]['data'][0]['data'] . ' ' . $data[0]['data'][1]['data'];
    return '<div class="w3-display-container">
        <img src="/oop.local/project/images/avatar.jpg" style="width:100%" alt="Avatar">
        <div class="w3-display-bottomleft w3-container w3-text-white">
            <h2>' . $name . '</h2>
        </div>
    </div>';
}

function preparePersonalInformation($data)
{
    $personalInformation = '';
    $personalInformationData = findDataBlock('personal_data', $data);
    $additionalData = $personalInformationData['additional_data'];
    foreach ($additionalData as $oneDataBlock) {
        $personalInformation .= '<p><i class="' . $oneDataBlock['attributes'] . '"></i>' . $oneDataBlock['data'] . '</p>';
    }

    return $personalInformation;
}

function prepareSkills($data)
{
    $skills = '<p class="w3-large"><b><i class="fa fa-asterisk fa-fw w3-margin-right w3-text-teal"></i>Skills</b></p>';

    $skillsArrayData = findDataBlock('skills', $data);
    $skillsData = $skillsArrayData['data'][0]['data'];
    $styleSkills = '</p><div class="w3-light-grey w3-round-xlarge w3-small"><div class="w3-container w3-center w3-round-xlarge w3-teal" style="width:';
    foreach ($skillsData as $skill) {
        $skillPercentage = $skill['percentage'] . '%';
        $skills .= '<p>' . $skill['title'] . $styleSkills ;
        $skills .= $skillPercentage . '">' . $skillPercentage . '</div></div>';
    }

    return $skills;
}

function prepareLanguages($data)
{
    $languages = '<p class="w3-large w3-text-theme"><b><i class="fa fa-globe fa-fw w3-margin-right w3-text-teal"></i>Languages</b></p>';
    $languagesData = $data[7]['data'][0]['data'];
    $styleLanguages = '</p><div class="w3-light-grey w3-round-xlarge"><div class="w3-container w3-center w3-round-xlarge w3-teal" style="height:24px;width:';
    foreach ($languagesData as $language) {
        $languagesPercentage = $language['percentage'] . '%';
        $languages .= '<p>' . $language['title'] . $styleLanguages ;
        $languages .= $languagesPercentage . '">' . $languagesPercentage . '</div></div>';
    }

    return $languages;
}

function prepareExperience($data)
{
    $experienceHtml = '<h2 class="w3-text-grey w3-padding-16"><i  class="fa fa-suitcase fa-fw w3-margin-right w3-xxlarge w3-text-teal"></i>Work Experience</h2>';
    $experienceData = findDataBlock('experience', $data);
    $experienceList = $experienceData['data'];
    foreach ($experienceList as $experience) {
        $experienceHtml .= '<div class="w3-container">';
        $experienceHtml .= '<h5 class="w3-opacity"><b>' . $experience['position'] . ' / ' . $experience['company'] . '</b></h5>';
        $experienceHtml .= '<h6 class="w3-text-teal"><i class="fa fa-calendar fa-fw w3-margin-right"></i>' . $experience['period'] . '</h6>';
        $experienceHtml .= '<p>' . $experience['role'] . '</p>';
        $experienceHtml .= '<hr>';
        $experienceHtml .= '</div>';
    }

    return $experienceHtml;
}

function prepareEducation($data)
{
    $educationHtml = '<h2 class="w3-text-grey w3-padding-16"><i class="fa fa-certificate fa-fw w3-margin-right w3-xxlarge w3-text-teal"></i>Education</h2>';
    $educationData = findDataBlock('education', $data);
    $educationList = $educationData['data'];
    foreach ($educationList as $education) {
        $educationHtml .= '<div class="w3-container">';
        $educationHtml .= '<h5 class="w3-opacity"><b>' . $education['institution'] . '</b></h5>';
        $educationHtml .= '<h6 class="w3-text-teal"><i class="fa fa-calendar fa-fw w3-margin-right"></i>' . $education['period'] . '</h6>';
        $educationHtml .= '<p>' . $education['specialty'] . '</p>';
        $educationHtml .= '<hr>';
        $educationHtml .= '</div>';
    }

    return $educationHtml;
}

function findDataBlock($what, $where)
{
    $findBlock = array();
    foreach ($where as $block) {
        foreach ($block as $key => $value) {
            if ($key == 'destination' && $value == $what) {
                $findBlock = $block;
                break;
            }
        }
    }

    return $findBlock;
}