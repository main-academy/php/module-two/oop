<?php
include 'Autoload.php';
spl_autoload_register('Autoload::loader');

use HtmlParser\Parser as htmlParser;
use CLIParser\Parser as cliParser;

$p = new htmlParser();
$c = new cliParser();

