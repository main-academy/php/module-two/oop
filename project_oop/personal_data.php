<?php

$personalData =
    [
    [
        'title' => 'Personal data',
        'destination' => 'personal_data',
        'data'  => [
            [
                'type' => 'firstname',
                'label' => 'First Name',
                'data' => 'Vitalii',
            ],
            [
                'type' => 'lastname',
                'label' => 'Last Name',
                'data' => 'Vasylyk',
            ],
            [
                'type' => 'dob',
                'label' => 'Date of Birthday',
                'data' => '28-07-1990',
            ],
        ],
        'additional_data' => [
            [
                'type' => 'vacancy',
                'label' => 'Vacancy',
                'data' => 'Backend Developer',
                'attributes' => 'fa fa-briefcase fa-fw w3-margin-right w3-large w3-text-teal'
            ],
            [
                'type' => 'address',
                'label' => 'Address',
                'data' => 'Ternopil, UA',
                'attributes' => 'fa fa-home fa-fw w3-margin-right w3-large w3-text-teal'
            ],
            [
                'type' => 'email',
                'label' => 'Email',
                'data' => 'vitalii@fakemail.com',
                'attributes' => 'fa fa-envelope fa-fw w3-margin-right w3-large w3-text-teal'
            ],
            [
                'type' => 'phone',
                'label' => 'phone',
                'data' => '087-234-12-12',
                'attributes' => 'fa fa-phone fa-fw w3-margin-right w3-large w3-text-teal'
            ]
        ]
    ],
    [
        'title' => 'Contacts',
        'destination' => 'contacts',
        'data' => [
            [
                'type' => 'facebook',
                'label' => 'Facebook',
                'data' => 'https://www.facebook.com/vitalii.vasylyk'
            ],
            [
                'type' => 'instagram',
                'label' => 'Instagram',
                'data' => 'https://www.instagram.com/vitalii.vasylyk'
            ],
            [
                'type' => 'twitter',
                'label' => 'Twitter',
                'data' => 'https://www.twitter.com/vitalii.vasylyk'
            ],
            [
                'type' => 'linkedin',
                'label' => 'Linked In',
                'data' => 'https://www.linkedin.com/in/vitalii.vasylyk',
            ]
        ]
    ],
    [
        'title' => 'Objective',
        'destination' => 'objective',
        'data' => [
            [
                'type' => 'text',
                'label' => '',
                'data' => 'Start a junior Backend developer career in eMagicOne Co. ' .
                    PHP_EOL . 'and in 5 years to grow to the team lead'
            ]
        ],
    ],
    [
        'title' => 'Summary',
        'destination' => 'summary',
        'data' => [
            [
                'type' => 'text',
                'label' => '',
                'data' => 'Experience in software development for over 2 years, the last two years ' . PHP_EOL .
                    'as a web-developer Javascript / HTML / CSS / Java. I also have experience ' . PHP_EOL .
                    'programming in C / C ++ / C #, PHP, AS3, SQL and works in graphics packages such ' . PHP_EOL .
                    'as Photoshop, CorelDraw and 3DStudio MAX.' . PHP_EOL .
                    'I have good analytical skills. Easy to train. I work well in a team'
            ]
        ]
    ],
    [
        'title' => 'Skills',
        'destination' => 'skills',
        'data' => [
            [
                'type' => 'item',
                'label' => 'Programming languages',
                'data' => [
                    [
                        'title' => 'PHP',
                        'percentage' => '90',
                    ],
                    [
                        'title' => 'ES 5/6',
                        'percentage' => '60',
                    ],
                    [
                        'title' => 'Java',
                        'percentage' => '40',
                    ],
                    [
                        'title' => 'Kotlin',
                        'percentage' => '30',
                    ],
                    [
                        'title' => 'Delphi',
                        'percentage' => '40',
                    ],
                ]
            ],
            [
                'type' => 'item',
                'label' => 'Databases',
                'data' => [
                    'MySQL',
                    'PostgreeSQL',
                    'Firebase',
                    'MongoDB',
                ]
            ],
            [
                'type' => 'item',
                'label' => 'Frontend',
                'data' => [
                    'HTML',
                    'CSS'
                ]
            ],
            [
                'type' => 'item',
                'label' => 'Servers',
                'data' => [
                    'Apache',
                    'Nginx'
                ]
            ],
            [
                'type' => 'item',
                'label' => 'Subversions',
                'data' => [
                    'Git',
                    'SVN'
                ]
            ],
            [
                'type' => 'item',
                'label' => 'Frameworks',
                'data' => [
                    'Yii 2',
                    'Zend Framework 3',
                    'CakePHP',
                    'Symfony',
                    'Laravel',
                    'Lumen',
                ]
            ],
            [
                'type' => 'item',
                'label' => 'OS',
                'data' => [
                    'Linux',
                    'Windows'
                ]
            ],
            [
                'type' => 'item',
                'label' => 'Other',
                'data' => [
                    'PHP Unit',
                    'XDebug',
                    'Ant',
                    'Maven',
                    'Docker',
                    'Jenkins',
                    'Jira',
                    'Trello'
                ]
            ]
        ]
    ],
    [
        'title' => 'Experience',
        'destination' => 'experience',
        'data' => [
            [
                'type' => 'list',
                'period' => 'August 2018 - Present',
                'company' => 'eMagicOne',
                'position' => 'Web Developer',
                'role' => 'Web applications/modules/extensions/scripts',
                'projects' => [
                    'CRM',
                    'API',
                    'CMS'
                ],
                'technologies' => [
                    'PHP 5.6/PHP 7.x',
                    'Javascript',
                    'PHP Unit',
                    'Composer',
                    'Magento/Prestashop and other e-commerce solutions'
                ]
            ],
            [
                'type' => 'list',
                'period' => 'October 2017 - August 2018',
                'company' => 'eMagicOne',
                'position' => 'Junior Web Developer',
                'role' => 'Web applications/modules/extensions/scripts',
                'projects' => [
                    'WEB Sites'
                ],
                'technologies' => [
                    'Joomla',
                    'WordPress',
                    'Open X',
                    'HTML',
                    'CSS'
                ]
            ]
        ]
    ],
    [
        'title' => 'Education',
        'destination' => 'education',
        'data' => [
            [
                'type' => 'list',
                'period' => '2007 - 2012',
                'institution' => 'Ternopil State \'Ivan Pul\'uj\' Technical University',
                'specialty' => 'Mechanical Engineering/Mechanical Technology/Technician, masterdegree'
            ],
            [
                'type' => 'list',
                'period' => 'June 2019 - August 2019',
                'institution' => 'Main Academy',
                'specialty' => 'PHP developer'
            ]
        ]
    ],
    [
        'title' => 'Additional information',
        'destination' => 'additional_information',
        'data' => [
            [
                'type' => 'item',
                'label' => 'Languages',
                'data' => [
                    [
                        'title' => 'Ukrainian (Native)',
                        'percentage' => '100',
                    ],
                    [
                        'title' => 'English (Intermediate)',
                        'percentage' => '60',
                    ],
                    [
                        'title' => 'Russian (Intermediate)',
                        'percentage' => '50'
                    ]
                ]
            ],
            [
                'type' => 'item',
                'label' => 'Hobbies',
                'data' => [
                    'Basketball',
                    'Crossfit',
                    'Gaming',
                    'Art/Design',
                    'Skateboarding',
                ]
            ]
        ]
    ]
];

return $personalData;
