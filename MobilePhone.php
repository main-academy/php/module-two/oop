<?php
/**
 * Created by PhpStorm.
 * User: vitwh
 * Date: 7/23/2019
 * Time: 7:28 PM
 */

class MobilePhone
{
    public $brand;
    public $model;
    public $camera;
    public $fingerPrint;
    public $display;
    public $usbPort;
    public $threeOFifePort;
    private $processor;
    private $memory;
    private $motherBoard;
    private $operationalMemory;

    /**
     * MobilePhone constructor.
     * @param $camera
     * @param $fingerPrint
     * @param $display
     * @param $usbPort
     * @param $threeOFifePort
     * @param $processor
     * @param $memory
     * @param $motherBoard
     * @param $operationalMemory
     */
    public function __construct(
        $camera,
        $fingerPrint,
        $display,
        $usbPort,
        $threeOFifePort,
        $processor,
        $memory,
        $motherBoard,
        $operationalMemory
    ) {
        $this->camera = $camera;
        $this->fingerPrint = $fingerPrint;
        $this->display = $display;
        $this->usbPort = $usbPort;
        $this->threeOFifePort = $threeOFifePort;
        $this->processor = $processor;
        $this->memory = $memory;
        $this->motherBoard = $motherBoard;
        $this->operationalMemory = $operationalMemory;
    }

    public function __destruct()
    {
        echo 'DESTUCTOR';
    }

    /**
     * @return mixed
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * @param mixed $brand
     */
    public function setBrand($brand)
    {
        $this->brand = $brand;
    }

    /**
     * @return mixed
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * @param mixed $model
     */
    public function setModel($model)
    {
        $this->model = $model;
    }

    /**
     * @return mixed
     */
    public function getCamera()
    {
        return $this->camera;
    }

    /**
     * @param mixed $camera
     */
    public function setCamera($camera)
    {
        if ($camera['size'] < '5') {
            $this->camera = $camera;
        }
    }

    /**
     * @return mixed
     */
    public function getFingerPrint()
    {
        return $this->fingerPrint;
    }

    /**
     * @param mixed $fingerPrint
     */
    public function setFingerPrint($fingerPrint)
    {
        if ($fingerPrint['human_part'] != 'leg') {
            $this->fingerPrint = $fingerPrint;
        }
    }

    /**
     * @return mixed
     */
    public function getDisplay()
    {
        return $this->display;
    }

    /**
     * @param mixed $display
     */
    public function setDisplay($display)
    {
        $this->display = $display;
    }

    /**
     * @return mixed
     */
    public function getUsbPort()
    {
        return $this->usbPort;
    }

    /**
     * @param mixed $usbPort
     */
    public function setUsbPort($usbPort)
    {
        $this->usbPort = $usbPort;
    }

    /**
     * @return mixed
     */
    public function getThreeOFifePort()
    {
        return $this->threeOFifePort;
    }

    /**
     * @param mixed $threeOFifePort
     */
    public function setThreeOFifePort($threeOFifePort)
    {
        $this->threeOFifePort = $threeOFifePort;
    }

    /**
     * @return mixed
     */
    public function getProcessor()
    {
        return $this->processor;
    }

    /**
     * @param mixed $processor
     */
    public function setProcessor($processor)
    {
        $this->processor = $processor;
    }

    /**
     * @return mixed
     */
    public function getMemory()
    {
        return $this->memory;
    }

    /**
     * @param mixed $memory
     */
    public function setMemory($memory)
    {
        $this->memory = $memory;
    }

    /**
     * @return mixed
     */
    public function getMotherBoard()
    {
        return $this->motherBoard;
    }

    /**
     * @param mixed $motherBoard
     */
    public function setMotherBoard($motherBoard)
    {
        $this->motherBoard = $motherBoard;
    }

    /**
     * @return mixed
     */
    public function getOperationalMemory()
    {
        return $this->operationalMemory;
    }

    /**
     * @param mixed $operationalMemory
     */
    public function setOperationalMemory($operationalMemory)
    {
        $this->operationalMemory = $operationalMemory;
    }

}