<?php

function prepareHeader($content)
{
    $content = strtoupper($content);
    return chr(27) . "[0;32m{$content}" . chr(27) . "[0m" . PHP_EOL;
}

function prepareContent($content)
{
    $data = '';

    foreach ($content as $line) {
        $label = isset($line['label']) ? prepareLabel($line['label']) : '';
        switch ($line['type']) {
            case 'item':
                $data .= $label . implode(', ', $line['data']) . PHP_EOL;
                break;
            case 'list':
                unset($line['type']);
                foreach ($line as $key => $value) {
                    $list = is_array($value) ? implode(', ', $value) : $value;
                    $data .= prepareLabel(ucfirst($key)) . $list . PHP_EOL;
                }
                $data .= PHP_EOL;
                break;
            default:
                $data .= $label . $line['data'] . PHP_EOL;
        }

    }

    return trim($data) . PHP_EOL . PHP_EOL;
}

function prepareLabel($content)
{
    return chr(27) . "[1m{$content}: " . chr(27) . "[0m";
}
