<?php

ini_set("display_errors", 1);
error_reporting(E_ALL);

require_once(__DIR__ . '/builder.php');

$structure = require_once(__DIR__ . '/data.php');
foreach ($structure as $data) {
    $header = prepareHeader($data['title']);
    $content = prepareContent($data['data']);

    ${$data['destination']} = "{$header}{$content}";
}

require_once(__DIR__ . '/template.php');
