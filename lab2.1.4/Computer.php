<?php

class Computer
{
    const IS_DESKTOP        = true;
    const IS_LAPTOP         = false;
    const ELECTRIC_DEVICE  = 'YES';
    const POWER             = '220V';

    public static $motherboard = '5"6\'';
    public static $lanCard;

    public $cpu;
    public $memory; // value
    public $ram;
    public $display;
    public $computerName = 'Computer';

    private $isWorking = false;

    public function turnOn()
    {
        if (self::$motherboard == '5"6\'') {
            $this->isWorking = true;
            echo 'turn on computer';
            echo '<br>';
        }
    }

    public function shutDown()
    {
        $this->isWorking = false;
        echo 'shutdown computer';
        echo '<br>';
    }

    public function reset()
    {
        if ($this->isWorking) {
            $this->shutDown();

            for ($timer = 4; $timer > 0; $timer--) {
                echo '.';
                sleep(2);
            }
            echo '<br>';

            $this->turnOn();
        }

        echo 'computer reset done';
    }

    public function printParameters()
    {
        if ($this->isWorking) {

            echo 'Computer is ' . $this->computerName;
            echo '<br>';
            echo 'CPU is ' . $this->cpu;
            echo '<br>';
            echo 'MEMORY is ' . $this->memory;
            echo '<br>';
            echo 'RAM is ' . $this->ram;
            echo '<br>';
            echo 'DISPLAY is ' . $this->display;
            echo '<br>';
        } else {
            echo 'COMPUTER NOT WORKING';
        }
    }

    public function identifyUser()
    {
        echo 'Computer: Identify by login and password';
    }
}