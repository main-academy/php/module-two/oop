<?php

class MacBook extends Computer
{
    const IS_DESKTOP = parent::IS_DESKTOP;

    /**
     * MacBook constructor.
     */
    public function __construct(
        $cpu,
        $memory,
        $display,
        $ram,
        $computerName
    ) {
        $this->cpu = $cpu;
        $this->memory = $memory;
        $this->display = $display;
        $this->ram = $ram;
        $this->computerName = $computerName;
    }
}