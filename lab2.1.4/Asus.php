<?php

class Asus extends Computer
{
    const IS_DESKTOP = parent::IS_DESKTOP;

    /**
     * Asus constructor.
     */
    public function __construct(
        $cpu,
        $memory,
        $display,
        $ram,
        $computerName
    ) {
        $this->cpu = $cpu;
        $this->memory = $memory;
        $this->display = $display;
        $this->ram = $ram;
        $this->computerName = $computerName;
    }

    public function identifyUser()
    {
        if (parent::$motherboard == '5"6\'') {
            echo "ASUS! and MB - " . parent::$motherboard;
        }
    }
}