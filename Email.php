<?php
/**
 * Work with email
 */


class Email
{
    var $test;
    public $user;

    protected $password;

    private $email = 'test@email.com';
    private $subject;

    const EMAIL = 'test@email.com';

    /**
     * Email constructor.
     */
    public function __construct($user, $password, $email, $subject)
    {
        $this->user = $user;
        $this->password = $password;
        $this->email = $email;
        $this->subject = $subject;
        $this->prepareDataBeforeRunNext();
        echo 'Constructor entered - ' . __CLASS__ . '<br>';
    }

    public function __call($method, $args)
    {
        echo "Called __call with $method","\n<br>\n";
    }

    public function __destruct()
    {
        echo 'The class "', __CLASS__, '" was destroyed.<br />';
    }

    public function __toString()
    {
        return "ТИ МЕНЕ ПОКЛИКАВ НАЧЕ Я СТРІЧКА А Я ОБЄКТ";
    }

    public function sendEmail()
    {
        echo 'User is: ' . $this->user;
        echo '<br>';
        echo 'Email: ' . $this->email;
        echo '<br>';
        echo 'Your password is: ' . $this->password;
        echo '<br>';
        echo 'Subject: ' . $this->subject;
        echo '<br>';
        echo 'Google Autherification';
        echo 'Email was sent';
        echo '<br>';
        echo '<br>';
    }

    public function setEmail($email)
    {
        self::testEmail();
        if ($this->checkDomain('google')) {
            $this->email = $email;
        }
    }

    public function getEmail()
    {
        return $this->email;
    }

    private function checkDomain($domain) {
        if ($domain == 'google') {
            return true;
        }

        return false;
    }

    protected function setPassword() {

    }

    public static function testEmail() {

    }
}