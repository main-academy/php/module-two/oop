<?php

interface Mother
{
    const HEIR = 'blonde';

    public function eat();

    public function drink();

    public function homeWork();
}