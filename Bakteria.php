<?php
/**
 * Created by PhpStorm.
 * User: vitwh
 * Date: 7/23/2019
 * Time: 9:53 PM
 */

interface Bakteria
{
    public function live();

    public function move();

    public function eat();
}