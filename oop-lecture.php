Для багатьох програмістів PHP, об'єктно-орієнтоване програмування - це лякаюча концепція, повна складного синтаксису та інших блоків.

Щоб дізнатись, що ви можете робити з об'єктно-орієнтованим PHP, перегляньте величезний діапазон скриптів PHP на CodeCanyon, таких як SQLite Object-Oriented Framework.

Або, якщо ви боретеся з підходом до себе, ви можете найняти професіонала в Envato Studio, щоб виправити помилки для вас або створити повні програми та модулі PHP.

Розуміння об'єктно-орієнтованого програмування
Об'єктно-орієнтоване програмування - це стиль кодування, який дозволяє розробникам групувати подібні завдання до класів. Це допомагає зберегти код, який слідує за принципом "не повторюй себе" (DRY) і простий у обслуговуванні.

"Об'єктно-орієнтоване програмування - це стиль кодування, який дозволяє розробникам групувати подібні завдання на заняття".
Однією з основних переваг програми DRY є те, що якщо частина інформації змінюється у вашій програмі, зазвичай одна зміна необхідна для оновлення коду. Однією з найбільших кошмарів для розробників є підтримка коду, де дані знову і знову оголошуються, а це означає, що будь-які зміни в програмі стають нескінченно більш розчаруванням гри Where's Waldo? оскільки вони полюють за дубльовані дані та функціональні можливості.

ООП залякує багатьох розробників, оскільки він вводить новий синтаксис, і, з першого погляду, виглядає набагато складніше, ніж просто процедурний або вбудований код. Проте, при більш детальному огляді, ООП насправді є дуже простим і, зрештою, простим підходом до програмування.

Розуміння об'єктів та класів
Перш ніж ви зможете заглибитися в тонкі точки ООП, необхідно розуміти основні відмінності між об'єктами та класами. У цьому розділі перейдуть будівельні блоки класів, їх різні можливості та деякі з них.

Визнання відмінностей між об'єктами та класами
Розробники починають говорити про об'єкти та класи, і вони, здається, є взаємозамінними умовами. Однак це не так.
Відразу біта, це незрозуміло в OOP: досвідчені розробники починають говорити про об'єкти та класи, і вони, здається, є взаємозамінними умовами. Це не так, однак, хоча різниця може бути важко обернути вашу голову на перший погляд.

Клас, наприклад, виглядає як схема будинку. Він визначає форму будинку на папері, причому відносини між різними частинами будинку чітко визначені і заплановані, навіть якщо будинок не існує.

Об'єкт, таким чином, виглядає як справжній будинок, побудований відповідно до цього плану. Дані, що зберігаються в об'єкті, подібні до дерева, дротів та бетону, що складають будинок: не будучи зібраним відповідно до креслення, це просто купа предметів. Однак, коли все це відбувається разом, воно стає організованим, корисним будинком.

Класи складають структуру даних і дій і використовують цю інформацію для побудови об'єктів. Більш ніж один об'єкт може бути побудований з одного і того ж класу одночасно, кожен незалежно від інших. Продовжуючи нашу аналогію з будівництвом, вона схожа на те, як цілий підрозділ може бути побудований за тією самою схемою: 150 різних будинків, які виглядають однаково, але мають різні сімейства та прикраси всередині.

Класи структурування
Синтаксис для створення класу досить простий: оголошення класу з використанням класу ключове слово, за яким слід назвати клас і набір фігурних дужок ({}):

<?php
class MyClass
{
  // Class properties and methods go here
}
?>

Після створення класу новий клас може бути екземплярований і зберігатися в змінній, використовуючи нове ключове слово:

<?php $obj = new MyClass; ?>
Щоб побачити вміст класу, використовуйте var_dump ():

<?php var_dump($obj); ?>
Випробуйте цей процес, поставивши весь попередній код у новий файл з назвою test.php у теці [your local]:

<?php
class MyClass
{
    // Class properties and methods go here
}
$obj = new MyClass;
var_dump($obj);
?>
Завантажте сторінку в свій веб-переглядач за адресою, і наведене нижче має відображатися:

object(MyClass)#1 (0) { }
У найпростішій формі ви тільки що завершили свій перший скрипт OOP.

Визначення властивостей класу
Для додавання даних до класу, властивостей або змінних класу використовуються. Ці роботи точно схожі на звичайні змінні, за винятком, що вони пов'язані з об'єктом, і тому їх можна отримати лише за допомогою об'єкта.

Щоб додати властивість MyClass, додайте до вашого сценарію наступний код:

<?php
class MyClass
{
  public $prop1 = "I'm a class property!";
}
$obj = new MyClass;
var_dump($obj);
?>PHPCopy
Ключове слово public визначає видимість ресурсу, про що ви дізнаєтесь трохи пізніше в цьому розділі. Далі властивість називається стандартним синтаксисом змінної, і призначається значення (хоча властивості класу не потребують початкового значення).

Щоб прочитати цю властивість і вивести її в браузер, посилайтеся на об'єкт, з якого слід читати, і властивість для читання:

<?php echo $obj->prop1; ?>
Оскільки кілька екземплярів класу можуть існувати, як індивідуальний об'єкт не посилається, сценарій не зможе визначити, який об'єкт слід читати. Використання стрілки (->) - це конструкція OOP, яка використовує наявні властивості та методи даного об'єкта.

Змініть скрипт у test.php, щоб прочитати властивість, а не скинути весь клас шляхом зміни коду, як показано на малюнку:

<?php
class MyClass
{
  public $prop1 = "I'm a class property!";
}
$obj = new MyClass;
echo $obj->prop1; // Output the property
?>
Перезавантаження вашого браузера тепер видає наступне:

I'm a class property!
Визначення методів класу
Методи - це класові функції. Індивідуальні дії, які об'єкт зможе виконати, визначаються в класі як методи.

Наприклад, щоб створити методи, які б встановлювали та отримували значення властивості класу $ prop1, додати до вашого коду наступне:

<?php
class MyClass
{
  private $prop1 = "I'm a class property!";
  public function setProperty($newval)
  {
      $this->prop1 = $newval;
  }

  public function getProperty()
  {
      return $this->prop1 . "<br />";
  }
}
$obj = new MyClass;
echo $obj->prop1;
?>
Примітка. OOP дозволяє об'єктам реєструватися за допомогою $this. Під час роботи в рамках методу використовуйте $this так само, як ви використовуєте ім'я об'єкта за межами класу.

Щоб використовувати ці методи, називайте їх так само, як звичайні функції, але, по-перше, посилайтеся на об'єкт, до якого вони належать. Прочитайте власність з MyClass, змініть її значення та знову прочитайте її, виконавши наступні зміни:

<?php
class MyClass
{
  public $prop1 = "I'm a class property!";
  public function setProperty($newval)
  {
      $this->prop1 = $newval;
  }
  public function getProperty()
  {
      return $this->prop1 . "<br />";
  }
}
$obj = new MyClass;
echo $obj->getProperty(); // Get the property value
$obj->setProperty("I'm a new property value!"); // Set a new one
echo $obj->getProperty(); // Read it out again to show the change
?>PHPCopy
Перезавантажте веб-переглядач, і ви побачите таке:

I'm a class property!
I'm a new property value!PHPCopy
"Сила ООП стає очевидною при використанні декількох екземплярів того самого класу".
<?php
class MyClass
{
  public $prop1 = "I'm a class property!";
  public function setProperty($newval)
  {
      $this->prop1 = $newval;
  }
  public function getProperty()
  {
      return $this->prop1 . "<br />";
  }
}
// Create two objects
$obj = new MyClass;
$obj2 = new MyClass;
// Get the value of $prop1 from both objects
echo $obj->getProperty();
echo $obj2->getProperty();
// Set new values for both objects
$obj->setProperty("I'm a new property value!");
$obj2->setProperty("I belong to the secondnamespace instance!");
// Output both objects' $prop1 value
echo $obj->getProperty();
echo $obj2->getProperty();
?>PHPCopy
Коли ви завантажуєте результати у ваш браузер, вони читаються наступним чином:

I'm a class property!
I'm a class property!
I'm a new property value!
I belong to the second instance!PHPCopy
Як ви можете бачити, OOP зберігає об'єкти як окремі об'єкти, що дозволяє легко розділити різні частини коду на невеликі, пов'язані з ними розшарування.

Магічні методи в OOP
Щоб полегшити використання об'єктів, PHP також надає ряд магічних методів або спеціальних методів, які викликаються при деяких звичайних дій у об'єктах. Це дозволяє розробникам виконувати ряд корисних завдань з відносною легкістю.

Використання конструкторів та руйнівників
Коли об'єкт інвенціонує, часто бажано встановити декілька речей прямо біта. Для цього PHP забезпечує магічний метод __construct (), який автоматично викликається кожного разу, коли новий об'єкт створено.

З метою ілюстрування концепції конструкторів додати конструктор до MyClass, який виведе повідомлення кожного разу, коли буде створено новий екземпляр класу:

<?php
class MyClass
{
  public $prop1 = "I'm a class property!";
  public function __construct()
  {
      echo 'The class "', __CLASS__, '" was initiated!<br />';
  }
  public function setProperty($newval)
  {
      $this->prop1 = $newval;
  }
  public function getProperty()
  {
      return $this->prop1 . "<br />";
  }
}
// Create a new object
$obj = new MyClass;
// Get the value of $prop1
echo $obj->getProperty();
// Output a message at the end of the file
echo "End of file.<br />";
?>PHPCopy
Примітка - __CLASS__ повертає ім'я класу, в якому він викликається; це те, що відомо як магічна константа. Є декілька магічних констант, про які ви можете прочитати в керівництві PHP.

Перезавантаження файлу у вашому браузері дасть наступний результат:

The class "MyClass" was initiated!
I'm a class property!
End of file.PHPCopy
Щоб викликати функцію, коли об'єкт знищено, магнітний метод __destruct () доступний. Це корисно для очищення класів (наприклад, закриття з'єднання з базою даних).

Виводить повідомлення, коли об'єкт знищено шляхом визначення магічного методу__destruct () у MyClass:

<?php
class MyClass
{
  public $prop1 = "I'm a class property!";
  public function __construct()
  {
      echo 'The class "', __CLASS__, '" was initiated!<br />';
  }
  public function __destruct()
  {
      echo 'The class "', __CLASS__, '" was destroyed.<br />';
  }
  public function setProperty($newval)
  {
      $this->prop1 = $newval;
  }
  public function getProperty()
  {
      return $this->prop1 . "<br />";
  }
}
// Create a new object
$obj = new MyClass;
// Get the value of $prop1
echo $obj->getProperty();
// Output a message at the end of the file
echo "End of file.<br />";
?>PHPCopy
Визначивши деструктор, перезавантаження тестового файлу призводить до наступного виходу:

The class "MyClass" was initiated!
I'm a class property!
End of file.
The class "MyClass" was destroyed.PHPCopy
"Коли закінчиться файл, PHP автоматично випускає всі ресурси".
Щоб явним чином викликати деструктор, ви можете знищити об'єкт, використовуючи thenfunction unset ():

<?php
class MyClass
{
  public $prop1 = "I'm a class property!";
  public function __construct()
  {
      echo 'The class "', __CLASS__, '" was initiated!<br />';
  }
  public function __destruct()
  {
      echo 'The class "', __CLASS__, '" was destroyed.<br />';
  }
  public function setProperty($newval)
  {
      $this->prop1 = $newval;
  }
  public function getProperty()
  {
      return $this->prop1 . "<br />";
  }
}
// Create a new object
$obj = new MyClass;
// Get the value of $prop1
echo $obj->getProperty();
// Destroy the object
unset($obj);
// Output a message at the end of the file
echo "End of file.<br />";
?>PHPCopy
Тепер, коли завантажується у вашому браузері, результат змінюється наступним чином:

The class "MyClass" was initiated!
I'm a class property!
The class "MyClass" was destroyed.
End of file.PHPCopy
Перетворення в рядку
Щоб уникнути помилки, якщо скрипт намагається вивести MyClass як рядок, використовується інший магічний метод __toString ().

Без __toString (), спроба вивести об'єкт як рядок призводить до фатальної помилки. Спробуйте використовувати echo для виведення об'єкта без магічного методу на місці:

<?php
class MyClass
{
  public $prop1 = "I'm a class property!";
  public function __construct()
  {
      echo 'The class "', __CLASS__, '" was initiated!<br />';
  }
  public function __destruct()
  {
      echo 'The class "', __CLASS__, '" was destroyed.<br />';
  }
  public function setProperty($newval)
  {
      $this->prop1 = $newval;
  }
  public function getProperty()
  {
      return $this->prop1 . "<br />";
  }
}
// Create a new object
$obj = new MyClass;
// Output the object as a string
echo $obj;
// Destroy the object
unset($obj);
// Output a message at the end of the file
echo "End of file.<br />";
?>PHPCopy
Це призводить до наступного:

The class "MyClass" was initiated!
Catchable fatal error: Object of class MyClass could not be converted to string in /Applications/XAMPP/xamppfiles/htdocs/testing/test.php on line 40PHPCopy
Щоб уникнути цієї помилки, додайте метод __toString ():

<?php
class MyClass
{
  public $prop1 = "I'm a class property!";
  public function __construct()
  {
      echo 'The class "', __CLASS__, '" was initiated!<br />';
  }
  public function __destruct()
  {
      echo 'The class "', __CLASS__, '" was destroyed.<br />';
  }
  public function __toString()
  {
      echo "Using the toString method: ";
      return $this->getProperty();
  }
  public function setProperty($newval)
  {
      $this->prop1 = $newval;
  }
  public function getProperty()
  {
      return $this->prop1 . "<br />";
  }
}
// Create a new object
$obj = new MyClass;
// Output the object as a string
echo $obj;
// Destroy the object
unset($obj);
// Output a message at the end of the file
echo "End of file.<br />";
?>PHPCopy
У цьому випадку, спроба перетворити об'єкт у рядок призводить до виклику методу getProperty (). Завантажте тестовий скрипт у свій браузер, щоб побачити результат:

The class "MyClass" was initiated!
Using the toString method: I'm a class property!
The class "MyClass" was destroyed.
End of file.PHPCopy
Порада. На додаток до магічних методів, обговорюваних у цьому розділі, доступні декілька інших. Повний список магічних методів наведено на сторінці довідника PHP.

Використання класової спадщини
Класи можуть наслідувати методи та властивості іншого класу за допомогою розширюваного ключового слова. Наприклад, щоб створити другий клас, який розширює MyClass і додає метод, до вашого тестового файлу слід додати наступне:

<?php
class MyClass
{
  public $prop1 = "I'm a class property!";
  public function __construct()
  {
      echo 'The class "', __CLASS__, '" was initiated!<br />';
  }
  public function __destruct()
  {
      echo 'The class "', __CLASS__, '" was destroyed.<br />';
  }
  public function __toString()
  {
      echo "Using the toString method: ";
      return $this->getProperty();
  }
  public function setProperty($newval)
  {
      $this->prop1 = $newval;
  }
  public function getProperty()
  {
      return $this->prop1 . "<br />";
  }
}
class MyOtherClass extends MyClass
{
  public function newMethod()
  {
      echo "From a new method in " . __CLASS__ . ".<br />";
  }
}
// Create a new object
$newobj = new MyOtherClass;
// Output the object as a string
echo $newobj->newMethod();
// Use a method from the parent class
echo $newobj->getProperty();
?>PHPCopy
Перезавантажуючи тестовий файл у вашому браузері, виводиться наступне:

The class "MyClass" was initiated!
From a new method in MyOtherClass.
I'm a class property!
The class "MyClass" was destroyed.PHPCopy
Перезапис спадкових властивостей і методів
Щоб змінити поведінку наявної властивості чи методу в новому класі, ви можете просто перезаписати його, оголосивши його знову в новому класі:

<?php
class MyClass
{
  public $prop1 = "I'm a class property!";
  public function __construct()
  {
      echo 'The class "', __CLASS__, '" was initiated!<br />';
  }
  public function __destruct()
  {
      echo 'The class "', __CLASS__, '" was destroyed.<br />';
  }
  public function __toString()
  {
      echo "Using the toString method: ";
      return $this->getProperty();
  }
  public function setProperty($newval)
  {
      $this->prop1 = $newval;
  }
  public function getProperty()
  {
      return $this->prop1 . "<br />";
  }
}
class MyOtherClass extends MyClass
{
  public function __construct()
  {
      echo "A new constructor in " . __CLASS__ . ".<br />";
  }
  public function newMethod()
  {
      echo "From a new method in " . __CLASS__ . ".<br />";
  }
}
// Create a new object
$newobj = new MyOtherClass;
// Output the object as a string
echo $newobj->newMethod();
// Use a method from the parent class
echo $newobj->getProperty();
?>PHPCopy
Це змінює вивід в браузері на:

A new constructor in MyOtherClass.
From a new method in MyOtherClass.
I'm a class property!
The class "MyClass" was destroyed.PHPCopy
Збереження функціональності оригінального методу під час перезапису методів
Щоб додати нову функціональність до успадкованого методу, зберігаючи вихідний метод недоторканим, використовуйте батьківське ключове слово з оператором дозволу обмеження (:):

<?php
class MyClass
{
  public $prop1 = "I'm a class property!";
  public function __construct()
  {
      echo 'The class "', __CLASS__, '" was initiated!<br />';
  }
  public function __destruct()
  {
      echo 'The class "', __CLASS__, '" was destroyed.<br />';
  }
  public function __toString()
  {
      echo "Using the toString method: ";
      return $this->getProperty();
  }
  public function setProperty($newval)
  {
      $this->prop1 = $newval;
  }
  public function getProperty()
  {
      return $this->prop1 . "<br />";
  }
}
class MyOtherClass extends MyClass
{
  public function __construct()
  {
      parent::__construct(); // Call the parent class's constructor
      echo "A new constructor in " . __CLASS__ . ".<br />";
  }
  public function newMethod()
  {
      echo "From a new method in " . __CLASS__ . ".<br />";
  }
}
// Create a new object
$newobj = new MyOtherClass;
// Output the object as a string
echo $newobj->newMethod();
// Use a method from the parent class
echo $newobj->getProperty();
?>PHPCopy
Це виводить результат як батьківського конструктора, так і конструктора нового класу:

The class "MyClass" was initiated!
A new constructor in MyOtherClass.
From a new method in MyOtherClass.
I'm a class property!
The class "MyClass" was destroyed.PHPCopy
Призначення видимості властивостей та методів
Для додаткового контролю над об'єктами, методами та властивостями призначено видимість. Це контролює, як і звідки можна отримати доступ до властивостей і методів. Є три видимі ключові слова: загальнодоступні, захищені та приватні. Крім видимості, метод або властивість можна оголосити як статичний, що дозволяє їм отримувати доступ без інстанцирування класу.

"Для додаткового контролю над об'єктами, методами та властивостями призначено видимість".
Примітка. Видимість - це нова функція, починаючи з PHP 5. Для отримання інформації про сумісність OOP з PHP 4 див. сторінку керівництва PHP.

Загальнодоступні властивості та методи
Всі методи та властивості, які ви використовували до цих пір, були загальнодоступними. Це означає, що їх можна отримати в будь-якому місці, як у межах класу, так і зовні.

Захищені властивості та методи
Коли властивість або метод оголошується захищеним, його можна отримати лише в класі або в класах нащадків (класів, які розширюють клас, що містить захищений метод).

Оголосити метод getProperty () як захищений у MyClass і спробувати отримати доступ до нього безпосередньо з-за меж класу:

<?php
class MyClass
{
  public $prop1 = "I'm a class property!";
  public function __construct()
  {
      echo 'The class "', __CLASS__, '" was initiated!<br />';
  }
  public function __destruct()
  {
      echo 'The class "', __CLASS__, '" was destroyed.<br />';
  }
  public function __toString()
  {
      echo "Using the toString method: ";
      return $this->getProperty();
  }
  public function setProperty($newval)
  {
      $this->prop1 = $newval;
  }
  protected function getProperty()
  {
      return $this->prop1 . "<br />";
  }
}
class MyOtherClass extends MyClass
{
  public function __construct()
  {
      parent::__construct();
echo "A new constructor in " . __CLASS__ . ".<br />";
  }
  public function newMethod()
  {
      echo "From a new method in " . __CLASS__ . ".<br />";
  }
}
// Create a new object
$newobj = new MyOtherClass;
// Attempt to call a protected method
echo $newobj->getProperty();
?>PHPCopy
Після спроби запустити цей скрипт з'являється така помилка:

The class "MyClass" was initiated!
A new constructor in MyOtherClass.
Fatal error: Call to protected method MyClass::getProperty() from context '' in /Applications/XAMPP/xamppfiles/htdocs/testing/test.php on line 55PHPCopy
Тепер створіть новий метод у MyOtherClass, щоб викликати метод getProperty ():

<?php
class MyClass
{
  public $prop1 = "I'm a class property!";
  public function __construct()
  {
      echo 'The class "', __CLASS__, '" was initiated!<br />';
  }
  public function __destruct()
  {
      echo 'The class "', __CLASS__, '" was destroyed.<br />';
  }
  public function __toString()
  {
      echo "Using the toString method: ";
      return $this->getProperty();
  }
  public function setProperty($newval)
  {
      $this->prop1 = $newval;
  }
  protected function getProperty()
  {
      return $this->prop1 . "<br />";
  }
}
class MyOtherClass extends MyClass
{
  public function __construct()
  {
      parent::__construct();
echo "A new constructor in " . __CLASS__ . ".<br />";
  }
  public function newMethod()
  {
      echo "From a new method in " . __CLASS__ . ".<br />";
  }
  public function callProtected()
  {
      return $this->getProperty();
  }
}
// Create a new object
$newobj = new MyOtherClass;
// Call the protected method from within a public method
echo $newobj->callProtected();
?>PHPCopy
Це породжує бажаний результат:

The class "MyClass" was initiated!
A new constructor in MyOtherClass.
I'm a class property!
The class "MyClass" was destroyed.PHPCopy
Приватні властивості та методи
Властивість або метод, визнаний приватним, доступний лише з того самого класу, який його визначає. Це означає, що навіть якщо новий клас розширює клас, який визначає приватну власність, це властивість або метод взагалі не доступні в дочірньому класі.

Щоб продемонструвати це, оголосити getProperty () приватним у MyClass та спробувати зателефонувати callProtected () fromMyOtherClass:

<?php
class MyClass
{
  public $prop1 = "I'm a class property!";
  public function __construct()
  {
      echo 'The class "', __CLASS__, '" was initiated!<br />';
  }
  public function __destruct()
  {
      echo 'The class "', __CLASS__, '" was destroyed.<br />';
  }
  public function __toString()
  {
      echo "Using the toString method: ";
      return $this->getProperty();
  }
  public function setProperty($newval)
  {
      $this->prop1 = $newval;
  }
  private function getProperty()
  {
      return $this->prop1 . "<br />";
  }
}
class MyOtherClass extends MyClass
{
  public function __construct()
  {
      parent::__construct();
      echo "A new constructor in " . __CLASS__ . ".<br />";
  }
  public function newMethod()
  {
      echo "From a new method in " . __CLASS__ . ".<br />";
  }
  public function callProtected()
  {
      return $this->getProperty();
  }
}
// Create a new object
$newobj = new MyOtherClass;
// Use a method from the parent class
echo $newobj->callProtected();
?>PHPCopy
Завантажте веб-переглядач і з'явиться така помилка:

The class "MyClass" was initiated!
A new constructor in MyOtherClass.
Fatal error: Call to private method MyClass::getProperty() from context 'MyOtherClass' in /Applications/XAMPP/xamppfiles/htdocs/testing/test.php on line 49PHPCopy
Статичні властивості та методи
Можна отримати доступ до методу або властивості, оголошеного статичним, без попереднього аналізу класу; ви просто поставляєте ім'я класу, оператор роздільної здатності смуги та ім'я властивості або методу.

"Одна з основних переваг використання статичних властивостей полягає в тому, що вони зберігають свої збережені значення протягом тривалості сценарію".
Щоб продемонструвати це, додайте статичне властивість з назвою $ count і статичний метод called plusOne () до MyClass. Потім встановіть робити..., поки цикл виведе збільшене значення $ count, якщо значення менше ніж 10:

<?php
class MyClass
{
  public $prop1 = "I'm a class property!";
  public static $count = 0;
  public function __construct()
  {
      echo 'The class "', __CLASS__, '" was initiated!<br />';
  }
  public function __destruct()
  {
      echo 'The class "', __CLASS__, '" was destroyed.<br />';
  }
  public function __toString()
  {
      echo "Using the toString method: ";
      return $this->getProperty();
  }
  public function setProperty($newval)
  {
      $this->prop1 = $newval;
  }
  private function getProperty()
  {
      return $this->prop1 . "<br />";
  }
  public static function plusOne()
  {
      return "The count is " . ++self::$count . ".<br />";
  }
}
class MyOtherClass extends MyClass
{
  public function __construct()
  {
      parent::__construct();
      echo "A new constructor in " . __CLASS__ . ".<br />";
  }
  public function newMethod()
  {
      echo "From a new method in " . __CLASS__ . ".<br />";
  }
  public function callProtected()
  {
      return $this->getProperty();
  }
}
do
{
  // Call plusOne without instantiating MyClass
  echo MyClass::plusOne();
} while ( MyClass::$count < 10 );
?>PHPCopy
Примітка. Коли ви отримуєте доступ до статичних властивостей, знак долара ($) надходить за оператором роздільної здатності.

__ Коли ви завантажуєте цей скрипт у свій браузер, виводиться наступне:

The count is 1.
The count is 2.
The count is 3.
The count is 4.
The count is 5.
The count is 6.
The count is 7.
The count is 8.
The count is 9.
The count is 10.PHPCopy
Коментування з DocBlocks
"Стиль коментування DocBlock - це широкомасштабний метод документування класів".
Хоча це не офіційна частина OOP, стиль коментування DocBlock є широко прийнятим методом документування класів. Окрім надання стандартним для розробників, який використовується при написанні коду, він також був прийнятий багатьма найпопулярнішими наборами програмного забезпечення (SDK), такими як Eclipse та NetBeans, і буде використовуватися для створення натяків коду.

DocBlock визначається за допомогою блочного коментарю, який починається з додаткового зірочка:

/**
 * This is a very basic DocBlock
 */PHPCopy
Реальна сила DocBlocks поставляється з можливістю використання тегів, які починаються з символу (@), після чого слідує ім'я тегу та значення тегу. Теги DocBlock дозволяють розробникам визначати авторів файлу, ліцензію на клас, інформацію про властивість або метод та іншу корисну інформацію.

Найбільш поширені теги використовуються наступним чином:

@ author: Автор цього елемента (який може бути класом, файлом, методом або будь-яким битом коду) перелічено за допомогою цього тегу. Кілька тегів автора можуть бути використані в тій самій DocBlock, якщо зараховується більше одного автора. Формат для імені автора - Джон Доу. @ copyright: це означає рік авторського права та ім'я власника авторських прав для поточного елемента. Формат 2010 Copyright Holder.@license: Це посилання на ліцензію на поточний елемент. Формат інформації про ліцензію - http: www.example.com - шлях до license.txt. Ліцензія Name.@var: це містить тип та опис зміненої властивості або класу. Формат - елемент типу description.@param: Цей тег показує тип та опис параметра функції або методу. Формат - це тип $ element_name element description.@return: Тип цього типу і опис поверненого значення функції або методу в цьому тезі. Формат - опис типу return element.

Приклад класу, коментував DocBlocks, може виглядати наступним чином:

<?php
/**
 * A simple class
 *
 * This is the long description for this class,
 * which can span as many lines as needed. It is
 * not required, whereas the short description is
 * necessary.
 *
 * It can also span multiple paragraphs if the
 * description merits that much verbiage.
 *
 * @author Jason Lengstorf <jason.lengstorf@euidesign.com>
 * @copyright 2010 Eui Desig
 * @license http://www.php.net/license/3_01.txt PHP License 3.01
 */
class SimpleClass
{
  /**
   * A public variable
   *
   * @var string stores data for the class
   */
  public $foo;
  /**
   * Sets $foo to a new value upon class instantiatio
   *
   * @param string $val a value required for the class
   * @return void
   */
  public function __construct($val)
  {
      $this->foo = $val;
  }
  /**
   * Multiplies two integers
   *
   * Accepts a pair of integers and returns the
   * product of the two.
   *
   * @param int $bat a number to be multiplied
   * @param int $baz a number to be multiplied
   * @return int the product of the two parameters
   */
  public function bar($bat, $baz)
  {
      return $bat * $baz;
  }
}
?>PHPCopy
Коли ви скануєте попередній клас, переваги DocBlock виявляються: все чітко окреслено, щоб наступний розробник міг підібрати код, і ніколи не доведеться замислюватися, що робить фрагмент коду чи що він повинен містити.

Порівняння об'єктно-орієнтованого та процесуального кодексу
Насправді не існує правильного і неправильного способу написання коду. Проте, у цьому розділі викладено сильний аргумент для прийняття об'єктно-орієнтованого підходу у розробці програмного забезпечення, особливо у великих програмах.

Причина 1: простий спосіб реалізації
"Хоча спочатку це може бути складним, OOP насправді забезпечує простий підхід до обробки даних".
Хоча спочатку це може бути складним, ООП насправді забезпечує простий підхід до обробка даних. Оскільки об'єкт може зберігати дані всередині, змінні не потрібно передавати від функції для функціонування належним чином.

Крім того, оскільки кілька екземплярів одного і того ж класу можуть існувати одночасно, спілкування з великими наборами даних нескінченно простіше. Наприклад, уявіть, що у файлі обробляється інформація про двох людей. Їм потрібні імена, професії та віки.

Процедурний підхід
Ось процедурний підхід до нашого прикладу:

<?php
function changeJob($person, $newjob)
{
  $person['job'] = $newjob; // Change the person's job
  return $person;
}
function happyBirthday($person)
{
  ++$person['age']; // Add 1 to the person's age
  return $person;
}
$person1 = array(
  'name' => 'Tom',
  'job' => 'Button-Pusher',
  'age' => 34
);
$person2 = array(
  'name' => 'John',
  'job' => 'Lever-Puller',
  'age' => 41
);
// Output the starting values for the people
echo "<pre>Person 1: ", print_r($person1, TRUE), "</pre>";
echo "<pre>Person 2: ", print_r($person2, TRUE), "</pre>";
// Tom got a promotion and had a birthday
$person1 = changeJob($person1, 'Box-Mover');
$person1 = happyBirthday($person1);
// John just had a birthday
$person2 = happyBirthday($person2);
// Output the new values for the people
echo "<pre>Person 1: ", print_r($person1, TRUE), "</pre>";
echo "<pre>Person 2: ", print_r($person2, TRUE), "</pre>";
?>PHPCopy
Коли виконується, код виводить наступне:

Person 1: Array
(
  [name] => Tom
  [job] => Button-Pusher
  [age] => 34
)
Person 2: Array
(
  [name] => Joh
  [job] => Lever-Puller
  [age] => 41
)
Person 1: Array
(
  [name] => Tom
  [job] => Box-Mover
  [age] => 35
)
Person 2: Array
(
  [name] => Joh
  [job] => Lever-Puller
  [age] => 42
)PHPCopy
Незважаючи на те, що цей код не обов'язково поганий, варто пам'ятати про кодування. Масив атрибутів постраждалих людей повинен бути переданий і повертатися з кожного виклику функції, що залишає поле для помилки.

Щоб очистити цей приклад, бажано залишити як можна менше розробника. Необхідно передати лише цілком необхідну інформацію для поточної операції.

Це місце, де йдеться, і допомагає вам очистити речі.

Підхід до ООП
Ось підхід ООП до нашого прикладу:

<?php
class Perso
{
  private $_name;
  private $_job;
  private $_age;
  public function __construct($name, $job, $age)
  {
      $this->_name = $name;
      $this->_job = $job;
      $this->_age = $age;
  }
  public function changeJob($newjob)
  {
      $this->_job = $newjob;
  }
  public function happyBirthday()
  {
      ++$this->_age;
  }
}
// Create two new people
$person1 = new Person("Tom", "Button-Pusher", 34);
$person2 = new Person("John", "Lever Puller", 41);
// Output their starting point
echo "<pre>Person 1: ", print_r($person1, TRUE), "</pre>";
echo "<pre>Person 2: ", print_r($person2, TRUE), "</pre>";
// Give Tom a promotion and a birthday
$person1->changeJob("Box-Mover");
$person1->happyBirthday();
// John just gets a year older
$person2->happyBirthday();
// Output the ending values
echo "<pre>Person 1: ", print_r($person1, TRUE), "</pre>";
echo "<pre>Person 2: ", print_r($person2, TRUE), "</pre>";
?>PHPCopy
У браузері виводиться наступне:

Person 1: Person Object
(
  [_name:private] => Tom
  [_job:private] => Button-Pusher
  [_age:private] => 34
)
Person 2: Person Object
(
  [_name:private] => Joh
  [_job:private] => Lever Puller
  [_age:private] => 41
)
Person 1: Person Object
(
  [_name:private] => Tom
  [_job:private] => Box-Mover
  [_age:private] => 35
)
Person 2: Person Object
(
  [_name:private] => Joh
  [_job:private] => Lever Puller
  [_age:private] => 42
)PHPCopy
Існує кілька додаткових налаштувань, які роблять підхід об'єктно-орієнтованим, але після визначення класу створюється і змінюється людина; інформація людини не повинна бути передана або повернена з методів, і лише кожна методика передає абсолютно необхідну інформацію.

"OOP значно зменшить вашу робочу навантаження, якщо вона буде застосована належним чином".
У невеликій масштабі ця різниця може здатися не настільки ж значною, але, оскільки ваші програми ростуть, OOP значно зменшить вашу робочу навантаження, якщо вона буде реалізована належним чином.

Tip - Не все повинно бути об'єктно-орієнтованим. Швидка функція, яка обробляє щось мале в одному місці всередині програми, не обов'язково повинна бути загорнута в клас. Використовуйте найкраще рішення при вирішенні між об'єктно-орієнтованими та процедурними підходами.

Причина 2: краща організація
Ще однією перевагою ООП є те, наскільки добре вона може бути легко упакована та каталогізована. Кожен клас, як правило, може зберігатися у власному окремому файлі, і якщо використовується однакова конвенція іменування, доступ до класів надзвичайно простий.

Припустимо, що у вас є додаток з 150 класами, які динамічно називаються через файл контролера в кореневій бібліотеці вашої файлової системи. Всі 150 класів слідують за класифікацією convening class.classname.inc.php і знаходяться в папці inc вашого додатка.

Контролер може реалізувати функцію PHP __autoload () для динамічного перетягування лише тих класів, які йому потрібні, як вони називаються, замість того, щоб включити всі 150 в файл контролера на всякий випадок або придумати якийсь розумний спосіб включення файлів у ваш власний код:

<?php
  function __autoload($class_name)
  {
      include_once 'inc/class.' . $class_name . '.inc.php';
  }
?>PHPCopy
Кожен клас в окремому файлі також робить код більш портативним і простішим для повторного використання в нових програмах без купа копіювання та вставки.

Причина 3: полегшення технічного обслуговування
. Через більш компактний характер ООП при правильному виконанні, зміни в коді, як правило, набагато простіше визначати і робити, ніж при довготривалому застосуванні коду парагемті.

Якщо певний масив інформації одержує новий атрибут, процедурний фрагмент програмного забезпечення може вимагати (у гіршому випадку), щоб новий атрибут додається до кожної функції, яка використовує масив.

Можливо, додаток OOP може бути оновлено так само, як легко додати нову властивість, а потім додати методи, які стосуються вказаного ресурсу.

. Багато переваг, наведених у цьому розділі, є продуктом ООП у поєднанні з практикою СУЧАСНОГО програмування. Безумовно, можна створити простий в обслуговуванні процедурний код, який не викликає кошмарів, і в рівній мірі можна створити жахливий об'єктно-орієнтований код. [Pro PHP та jQuery] намагатимуться продемонструвати поєднання хороших звичок кодування в поєднанні з OOP для створення чистого коду, який легко читати та підтримувати.

Резюме
У цей момент ви повинні відчувати себе комфортно з об'єктно-орієнтованим стилем програмування. Вивчення OOP - це чудовий спосіб прийняти ваш програму на наступний рівень. При правильному застосуванні OOP допоможе вам створювати зручний для читання, простий у обслуговуванні, портативний код, який заощадить вас (і розробників, які працюють з вами) годин додаткової роботи. Ви стискаєтесь з тим, що не було розглянуто в цій статті? Ви вже використовуєте OOP і маєте кілька порад для початківців? Поділіться ними в коментарях!

І якщо ви хочете придбати доступні, якісні PHP скрипти, ви можете знайти тисячі з них на CodeCanyon.

Примітка автора - Цей підручник був фрагментом з Pro PHP та jQuery (Apress, 2010).

КУРСЫ ВИДЕОУРОКИ СТУДЕНТАМ БЛОГ О КОМПАНИИ КОНТАКТЫ
МОВА САЙТУ: УКРАЇНСЬКА | РОСІЙСЬКА
+38 (050) 706-81-43
info@php-academy.kiev.ua
Киев, ул. Михаила Коцюбинского, 14
 
О КОМПАНИИ
PHP Academy - школа веб-программирования №1 в Украине. Мы предоставляем курсы с документальной гарантией стажировки, прописанной в договоре. Узнать больше