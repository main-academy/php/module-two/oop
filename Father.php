<?php

interface Father
{
    const EYES = 'brown';

    public function live();

    public function move();

    public function see();
}